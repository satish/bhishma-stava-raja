# Bhishma-stava-raja

The "king of hymns", a glorification of Bhagavan Sri Krishna, composed by Bhishma pitamaha in
Mahabharata Rajadharma-parva of Shanti-parva (Book 12).

This repository contains the "purer" Southern Recension of the hymn, based primarily on the
Malayalam manuscripts.

- Gita Press Gorakhpur (Nilakantha bhasya): 12.47.16 to 12.47.100 (plus ~30 *dākṣiṇātya* ṡlōkas)
- BORI-Poona critical edition (CE): 12.47.10 to 12.47.63
- Krishnacharya-Vyasacharya's Kumbakonam composite South Indian Recension: 12.46.17 to 12.46.132
- P.P.S Sastri's Madras-Southern Recension: 12.42.12 to 12.42.115
- [Kisari Mohan Ganguli](https://www.sacred-texts.com/hin/m12/m12a047.htm) and Manmatha Nath Dutt: 12.47


Telugu-Grantha (TG) recension offers the _longest_ known version of the Mahabharata. Malayalam (M)
offers a "purer", shorter Southern recension which has less additions and diversions compared to TG
manuscripts. The Northern manuscripts are in Bengali (B), Devanagari (D) and Kashmiri (K).

The idea here is to reproduce a distinctly Southern recension following along the M group with some
help from TG. When M and TG are a disjoint set, union with the Eastern (E) recension (esp. Bengali)
is preferred.

**Every word in the constituted text must be present in at least one Malayalam manuscript**.

Out of the ten Southern mss., if (TG + M≥1) mss. (i.e., at least seven mss.) provide a _uniform_
reading, then that majority reading is chosen. All such instances are noted in the footnotes with a
dagger (†).

When the constituted text is completely different from the M. group, it is shown with a double
dagger (‡).

Starred passages in CE are numbered like CE@809.  
Uncertain and inconclusive readings are denoted by ?.
