# Bhīṣma-stava-rājaḥ

>    janamējaya uvāca  
> ṡaratalpē ṡayānas tu ¹bharatānāṃ pitāmahaḥ  
> katham utsṛṣṭavān dēhaṃ kaṃ ca yōgam adhārayat  

¹ M T1 G1 Dn CE; T2 G2,4 PPS "bhāratānāṃ"

>    vaiṡaṃpāyana uvāca  
> ṡṛṇuṣvāvahitō rājan̄ ṡucir bhūtvā samāhitaḥ  
> bhīṣmasya kuruṡārdūla ¹dēhōtsargaṃ mahātmanaḥ  

¹ all; M3,4 "dēhōtsaṅgaṃ"

> ¹nivṛttamātrē tv ayana uttarē vai divākarē  
> samāvēṡayad ātmānam ātmany ēva samāhitaḥ  

¹ M G4 Dn CE; T G1,4 PPS "pravṛtta-"

> vikīrṇāṃṡur ivādityō bhīṣmaḥ ṡaraṡataiṡ citaḥ  
> ṡiṡyē paramayā lakṣmyā vṛtō brāhmaṇa¹sattamaiḥ  

¹ all; M4 "puṅgavaiḥ"

> vyāsēna ¹dēvaṡravasā nāradēna surarṣiṇā  
> dēvasthānēna vātsyēna ²tathā’ṡmēna sumantunā  

¹ M1,3,4 T G PPS; M2 CE "vēda-"; Dn "vēdaviduṣā"  
² M T G PPS; Dn CE "tathāṡmakasumantunā"

<!-- M2 B D Dn K2,4,5 V1 in. CE@65 (2 lines) and CE@66 (11 lines) here but T G M1,3,4 CE PPS om.  -->

> ētaiṡ cānyair munigaṇair mahābhāgair mahātmabhiḥ  
> ṡraddhādamapuraskārair vṛtaṡ candra iva grahaiḥ  

> bhīṣmas tu puruṣavyāghraḥ karmaṇā manasā girā  
> ṡaratalpagataḥ kṛṣṇaṃ ¹pradadhyau prān̄jaliḥ ṡuciḥ  

¹ all; M2 "prabhakṣyā"  
² M T G PPS; Dn CE "sthitaḥ"

> ¹svarēṇātha supuṣṭēna tuṣṭāva madhusūdanam  
> yōgēṡvaraṃ padmanābhaṃ viṣṇuṃ jiṣṇuṃ jagatpatim  

¹ M T G PPS; Dn "svarēṇa hṛṣṭapuṣṭēna" CE "svarēṇa puṣṭanādēna"

> kṛtān̄jaliḥ ṡucir bhūtvā vāgvidāṃ pravaraḥ ¹prabhuḥ  
> bhīṣmaḥ paramadharmātmā vāsudēvam athāstuvat  

¹ M2,4 T G2,4 B Dn PPS; M1,3 G1 CE "prabhum"

>    ¹bhīṣma uvāca  
> ārirādhayiṣuḥ kṛṣṇaṃ vācaṃ ²jigadiṣāmi yām  
> tayā vyāsasamāsinyā prīyatāṃ puruṣōttamaḥ /1/  

¹ M2,3,4 T1 G4 B D K D Dn PPS Cv; M1 CE om.  
² M1,2,4 T1 G1 Dn PPS; M3 CE "jigamiṣāmi"

> ¹ṡuciṃ ²ṡuciṣadaṃ haṃsaṃ ³paramaṃ paramēṣṭhinam  
> ⁴yuktvā sarvātmanātmānaṃ taṃ prapadyē ⁵prajāpatim /2/  

¹ M T G Dn PPS; CE "ṡuciḥ"  
² all; Dn "ṡucipadaṃ"  
³ M1,3,4; M2 T G1,24 PPS "tatparaṃ"; CE "tatparaḥ"; Dn "tatpadaṃ"  
⁴ M1,3,4 T1 G1,3,4 CE; M2 G2 "bhuktvā"; T2 "yuktā"; PPS "yuṅktvā"  
⁵ all; M4 "jagatpatim"

> anādyantaṃ paraṃ brahma na dēvā ¹narṣayō viduḥ  
> ²ēkō’yaṃ vēda bhagavān dhātā nārāyaṇō hariḥ /3/  

¹ all; M2 T2 "ṛṣayō"; M4 "nṛpayō"  
² M G2,3 Dn CE; T G1,4 PPS "ēkas tad vēda"; GP "ēkō yaṃ"

> nārāyaṇād ṛṣigaṇās tathā ¹siddhāmahōragāḥ  
> dēvā dēvarṣayaṡ caiva yaṃ viduḥ ²duḥkhabhēṣajam /4/  

¹ M1,3,4 G4; et al. "siddha-"  
² M T G; CE Dn "param avyayam"  

As per sandhi rules, siddhāḥ + mahōragāḥ = siddhāmahōragāḥ.

> dēvadānava¹gandharvā yakṣarakṣōmahōragāḥ  
> yaṃ na jānanti kō hy ēṣa kutō vā bhagavān iti /5/  

¹ M2,4 T G1,2,4 PPS; M1,3 "-gandharvayakṣarākṣasakinnarāḥ";  
  CE Dn "-gandharvā yakṣarākṣasapannagāḥ"

After /5/, T G1,2,4 PPS in. CE@73 (L3,L4) but M CE Dn om.

> yasmin viṡvāni bhūtāni tiṣṭhanti ¹ca viṡanti ca  
> guṇabhūtāni bhūtēṡē sūtrē maṇigaṇā iva /6/  

¹ ‡CE Dn; M1,3 "ca naṡati ca"; M2 "vinaṡanti ca"; M4 "pralayati ca";
  PPS "praviṡanty api"; T G2,4 "praviṡanti ca";

    NOTE: The following order of CE verses is followed below from /7/ to //:
    22 - 13 to 21 - 23 to 26 - 30 - 27 to 29 - 31 to 36 - 44 - 42 - 37 to 41 -
    43 - 46 - 45 - 50 - 47 - 49 - 51 - 88* - 48 - 53 - 54 - 55 (=PPS 104)

> yaṃ vai viṡvasya kartāraṃ jagatas tasthuṣāṃ patim  
> vadanti jagatō’dhyakṣam akṣaraṃ paramaṃ padam /7/  

> yasmin nityē tatē tantau dṛḍhē srag iva tiṣṭhati  
> sadasadgrathitaṃ viṡvaṃ viṡvāṅgē viṡvakarmaṇi /8/  

> hariṃ sahasraṡirasaṃ sahasracaraṇēkṣaṇam  
> ¹prāhur nārāyaṇaṃ dēvaṃ yaṃ viṡvasya parāyaṇam /9/  

¹ After /9L1/ Dn PPS B D in. CE@71 but M T G CE om.

> aṇīyasām aṇīyāṃsaṃ sthaviṣṭhaṃ ca sthavīyasām  
> garīyasāṃ gariṣṭhaṃ ca ṡrēṣṭhaṃ ca ṡrēyasām api /10/  

> yaṃ vākēṣv anuvākēṣu niṣatsūpaniṣatsu ca  
> ¹gṛṇanti satyakarmāṇaṃ satyaṃ satyēṣu sāmasu /11/  

¹ ?CE PPS Dn; PPS* "gṛhṇanti satyadharmāṇaṃ" or "gṛṇanti satyadharmāṇaṃ"

> caturbhiṡ caturātmānaṃ sattvasthaṃ sātvatāṃ patim  
> yaṃ divyair ¹dēvam arcanti guhyaiḥ paramanāmabhiḥ /12/  

¹ ?CE Dn; PPS "param arcanti"

> yaṃ dēvaṃ dēvakī dēvī vasudēvād ajījanat  
> ¹bhaumasya brahmaṇō guptyai dīptam agnim ivāraṇiḥ /13/  

¹ ?CE Dn PPS*; PPS "gōptāraṃ brahmaṇō yasya"

> yam ananyō vyapētāṡīr ātmānaṃ vītakalmaṣam  
> iṣṭvānantyāya gōvindaṃ paṡyaty ¹ātmany avasthitam  

¹ ?CE; PPS Dn "ātmānam ātmani"

> purāṇē ¹puruṣaḥ ¹prōktō ¹brahmā ¹prōktō yugādiṣu  
> kṣayē ¹saṃkarṣaṇaḥ ¹prōktas tam upāsyam upāsmahē  

¹ ?CE; PPS Dn end with anusvāra, as in "puruṣaṃ prōktaṃ brahma ... saṃkarṣaṇaṃ ..."

<!-- CE 20 PPS 28 ends here -->
<!-- all except M in. CE@73 here -->
<!-- PPS 33 = CE 21 continues below. "atha bhīṣma-stava-rājaḥ" -->

> ativāyvindrakarmāṇam atisūryāgnitējasam  
> atibuddhīndriyātmānaṃ taṃ prapadyē ¹prajāpatim  

¹ all; M1,3,4 "jagatpatim"

> hiraṇyavarṇaṃ yaṃ garbham ¹aditir daitya²nāṡanam  
> ēkaṃ dvādaṡadhā jajn̄ē tasmai sūryātmanē namaḥ  

¹ all; M2 "aditiṃ"; K B "aditēr"  
² all; M4 "-ṡāmanam"

> ṡuklē dēvān pitṝn kṛṣṇē tarpayaty amṛtēna yaḥ  
> yaṡ ca rājā dvijātīnāṃ tasmai sōmātmanē namaḥ  

<!-- T G1.2.4 PPS in. CE@74 here -->

> mahatas tamasaḥ pārē puruṣaṃ ¹hy atitējasam  
> yaṃ jn̄ātvā mṛtyum atyēti tasmai jn̄ēyātmanē namaḥ  

¹ M T G B Dn PPS; CE "jvalanadyutim"

> yaṃ bṛhantaṃ ¹bṛhaty ukthē yam agnau yaṃ mahādhvarē  
> yaṃ viprasaṃghā gāyanti tasmai vēdātmanē namaḥ  

¹ B Dn CE; M1,3,4 "bṛhaty uthyē"; T1 "bṛhaty ukthyē"; PPS "mahaty ukthē"; M2 om. verse

> ¹padāṅgaṃ saṃdhiparvāṇaṃ svaravyan̄jana²bhūṣaṇam  
> yam āhur akṣaraṃ nityaṃ tasmai vāgātmanē namaḥ  

¹ ‡T2 G1,3,4 CE; B Dn PPS "pādāṅgaṃ"; T1 M1,3,4 "padāgaṃ" (?); M2 om. verse  
² M T2 G3,4 Dn PPS; T1 G2 "-bhūṣitam"; G1 CE "-lakṣaṇam"

CONTINUE HERE CE 27 to 29:

> ¹ṛgyajuḥsāmadhāmānaṃ daṡārdhahavirākṛtim  
> yaṃ saptatantuṃ tanvanti tasmai yajn̄ātmanē namaḥ //  

¹ M2 om. verse

> yaḥ suparṇō yajur ¹nāma chandōgātras trivṛcchirāḥ  
> ²rathaṃtaraṃ ³bṛhatpakṣas tasmai stōtrātmanē namaḥ  

¹ M2 T G CE PPS; M1,3,4 "nāmnā"  
² M1,2,3 T2 G1,2 Dn; M4 T1 CE PPS "rathaṃtara-"  
³ M T G PPS; CE "-bṛhatyakṣas"; Dn "bṛhatsāma"

> yaḥ sahasrasavē satrē jajn̄ē viṡvasṛjām ṛṣiḥ  
> ¹hiraṇyapakṣaḥ ṡakunis tasmai ²tārkṣyātmanē namaḥ  

¹ M T G Dn PPS; CE "hiraṇyavarṇaḥ"  
² M T G PPS; Dn CE "haṃsātmanē"

> yaṡ cinōti satāṃ sētum ṛtēnāmṛtayōninā  
> dharmārthavyavahārāṅgais tasmai satyātmanē namaḥ  

> yaṃ pṛthagdharmacaraṇāḥ pṛthagdharmaphalaiṣiṇaḥ  
> pṛthagdharmaiḥ samarcanti tasmai dharmātmanē namaḥ  

> ¹yaṃ taṃ vyaktastham avyaktaṃ vicinvanti maharṣayaḥ  
> kṣētrē kṣētrajn̄am āsīnaṃ tasmai kṣētrātmanē namaḥ  

¹ M1,3,4 T1 G1 PPS CE; B M2 "yat tad"; T2 "yaṃ tu"; Dn "yaṃ ca"

> yaṃ ¹dṛgātmānam ātmasthaṃ vṛtaṃ ṣōḍaṡabhir guṇaiḥ  
> prāhuḥ ²saptadaṡaṃ sāṃkhyās tasmai sāṃkhyātmanē namaḥ  

¹ all; B Dn "tridhātmānam"  
² M4 T G CE PPS; M2 "sāptadaṡaṃ"; M1,3 "sāptadaṡāṃ"

> yaṃ vinidrā jitaṡvāsāḥ ¹santuṣṭās saṃyatēndriyāḥ  
> jyōtiḥ paṡyanti yun̄jānās tasmai yōgātmanē namaḥ  

¹ M T1 G1,2,4 PPS; T2 G3 Dn CE "sattvasthāḥ"

> apuṇyapuṇyōparamē yaṃ punarbhavanirbhayāḥ  
> ṡāntāḥ saṃnyāsinō yānti tasmai mōkṣātmanē namaḥ  

<!-- PPS 49 = Dn 55 = CE 36 ends above verse -->

> yasyāgnir āsyaṃ dyaur mūrdhā khaṃ nābhiṡ ¹caraṇau kṣitiḥ  
> sūryaṡ cakṣur diṡaḥ ²ṡrōtraṃ tasmai lōkātmanē namaḥ  

¹ all; M1,3 "caraṇāṃ"  
² M T G PPS; et al. "ṡrōtrē"

> yugēṣv āvartatē ¹yō’ṃṡair ²māsartvayanahāyanaiḥ  
> sargapralayayōḥ kartā tasmai kālātmanē namaḥ  

¹ M1,3,4 T G PPS CE; M2 Dn "yōgair"  
² M T1 G Dn PPS; T2 "māsairayana-"; CE "dinartva-"

> yō’sau yugasahasrāntē ¹pradīptārcir vibhāvasuḥ  
> saṃbhakṣayati bhūtāni tasmai ghōrātmanē namaḥ  

¹ all; M2 "pradiptāgnir"

> saṃbhakṣya sarvabhūtāni kṛtvā caikārṇavaṃ jagat  
> bālaḥ svapiti yaṡ caikas tasmai māyātmanē namaḥ  

> ¹sahasraṡirasē ²tasmai puruṣāyāmitātmanē  
> catuḥsamudra³paryāya⁴yōganidrātmanē namaḥ  

¹ M1,3,4 G4 om. verse  
² all; B Dn "caiva"  
³ M2 T2 Dn CE; T1 G1,2 PPS "-payasi yōga-"
⁴ all; M2 "-yōgi-"

> ajasya nābhāv adhyēkaṃ yasmin viṡvaṃ pratiṣṭhitam  
> ¹puṣkaraṃ puṣkarākṣasya tasmai padmātmanē namaḥ  

¹ M T1 G1,4 CE PPS; T2 G2 Dn "puṣkarē"

> yasya kēṡēṣu jīmūtā nadyaḥ sarvāṅgasaṃdhiṣu  
> kukṣau samudrāṡ catvāras tasmai tōyātmanē namaḥ  

<!-- all except M G4 CE in. CE@79 here (10 lines), total 29 out of 34 mss.
     PPS in. only L1,2 (PPS 57) and L5,6 (PPS 58).
     Dn in. all L1-10 (see Dn 61-65)
-->

> brahma vaktraṃ bhujau kṣatraṃ kṛtsnam ūrūdaraṃ viṡaḥ  
> pādau yasyāṡritāḥ ṡūdrās tasmai varṇātmanē namaḥ  

> annapānēndhanamayō rasaprāṇavivardhanaḥ  
> yō dhārayati bhūtāni tasmai prāṇātmanē namaḥ  

<!-- all except M G4 in. CE@80 here (6 lines), total 29 out of 34 mss. -->

> viṣayē vartamānānāṃ yaṃ taṃ ¹vaiṣayikair guṇaiḥ  
> prāhur viṣayagōptāraṃ tasmai gōptrātmanē namaḥ  

¹ M1,3,4 T G PPS; M2 CE "vaiṡēṣikair"

> apramēyaṡarīrāya sarvatō ¹buddhicakṣuṣē  
> apāra²parimāṇāya tasmai ³divyātmanē namaḥ  

¹ M T G Dn PPS; CE "-’nantacakṣuṣē"  
² M1,2,3 T G1,4 PPS; M4 "-pariṇāmāya"; Dn CE "-parimēyāya"  
³ M2,3 T G Dn PPS; M1,2 "vidyā-"; CE "cinty-"

> paraḥ kālāt parō yajn̄āt ¹paraḥ sadasataṡ ca yaḥ  
> anādir ādir viṡvasya tasmai viṡvātmanē namaḥ  

¹ M T2 G2 PPS; CE "paraḥ sadasatōṡ"; B Dn "parātparataṡ ca"

<!-- PPS in. CE@80L5-6 (29 out of 35 mss.) and CE@83 here -->

> ātmajn̄ānam idaṃ jn̄ānaṃ jn̄ātvā pan̄casv avasthitam  
> yaṃ ¹jn̄ānēnādhigacchanti tasmai jn̄ānātmanē namaḥ  

¹ M T G B Dn PPS; CE "jn̄āninō’-"

> jaṭinē daṇḍinē nityaṃ lambōdaraṡarīriṇē  
> kamaṇḍaluniṣaṅgāya tasmai brahmātmanē namaḥ  

> ¹ṡiraḥkapālamālāya vyāghracarmanivāsinē  
> ²bhasmadigdhaṡarīrāya tasmai rudrātmanē namaḥ  

¹ M T G PPS; Dn CE "ṡūlinē tridaṡēṡāya tryambakāya mahātmanē"  
² M T G PPS; Dn CE "bhasmadigdhōrdhvaliṅgāya"

> yō mōhayati bhūtāni snēha¹pāsānubandhanaiḥ  
> sargasya rakṣaṇārthāya tasmai mōhātmanē namaḥ   

¹ all; CE "-rāgānu-"

> pan̄cabhūtātmabhūtāya bhūtādi¹nidhanāya ca  
> akrōdhadrōhamōhāya tasmai ṡāntātmanē namaḥ  

¹ M T G PPS; Dn CE "-nidhanātmanē"

> yasmin sarvaṃ yataḥ sarvaṃ yaḥ sarvaṃ sarvataṡ ca yaḥ  
> yaṡ ca sarvamayō ¹dēvas tasmai sarvātmanē namaḥ  

¹ M T B G2,4 PPS; G1 "vēdas"; Dn CE "nityaṃ"

<!-- PPS in. subset of App.I No.6 here, PPS 76 to PPS 103, 56 lines. Dn M CE om. -->
<!-- M2 + 24 mss. in. CE@76 (4 lines) and 5 mss. M2 T G1,2 in. CE@54 (2 lines) here -->

> ¹viṡvakarman namas tē’stu viṡvātman viṡvasaṃbhava  
> ²apavargasthabhūtānāṃ pan̄cānāṃ ³parataḥ sthitaḥ  

¹ all; M2 "viṡvakarmaṃ"  
² M T1 G2,4 B Dn B PPS; CE "²apavargō’si bhū-"  
³ M2,4 T G3,4 CE; M1,3 "parataṃ"; G1,2 PPS "paramāsthitaḥ"

> namas tē triṣu lōkēṣu namas tē paratastriṣu  
> namas tē dikṣu sarvāsu tvaṃ hi ¹sarvaparāyaṇam  

¹ ?T1 G1,3,4 CE PPS; M2 G2 "-parāyaṇaḥ"; T2 "viṡvaparāyaṇaḥ"; M1,3,4 "-parāyaṇam" (?);  
  B Dn "sarvamayō nidhiḥ"

Above // is same as 12.51.4

> namas tē bhagavan viṣṇō lōkānāṃ ¹prabhavāpyaya  
> tvaṃ hi kartā hṛṣīkēṡa saṃhartā ²cāparājitaḥ  

¹ M1,2 T2 G1,2,3 CE PPS; M3,4 G4 "-vāvyaya"; T1 "-pyayaḥ"  
² all; M1,3,4 "cāparājita"

Above //is same as 12.51.2

> tēna paṡyāmi tē divyān bhāvān hi triṣu vartmasu  
> ¹tvāṃ ca ²paṡyāmi tattvēna yat tē rūpaṃ sanātanam  

¹ M; et al. "tac ca"  
² all; M2 "vakṣyāmi"  
³ all; M1,3,4 "rūpēṇa"

Above // is same as 12.51.5c-6b

> divaṃ tē ṡirasā vyāptaṃ padbhyāṃ dēvī vasuṃdharā  
> vikramēṇa trayō lōkāḥ puruṣō’si sanātanaḥ  

> atasīpuṣpasaṃkāṡaṃ pītavāsasam acyutam  
> yē namasyanti gōvindaṃ na tēṣāṃ vidyatē bhayam  

<!-- B Dn in CE@90 here (2 lines), T G1,2 PPS in CE@91 here (2 lines) -->
Above /CE60/ is 12.51.8ab

> nārāyaṇaṃ sahasrākṣaṃ sarvalōkanamaskṛtam  
> hiraṇyanābhaṃ ¹yajn̄āṅgam amṛtaṃ viṡvatōmukham  

¹ M2,4; M1,3 "viṡvāṅgam"

Above // is CE@95

> ¹nārāyaṇaparaṃ brahma nārāyaṇaparaṃ tapaḥ  
> nārāyaṇaparaṃ ²satyaṃ ³nārāyaṇaparaṃ param  

¹ M1,3 T G PPS CE; M2,4 Dn "nārāyaṇaḥ pa-"  
² M T G PPS; Dn "dēvaḥ"; CE "cēdaṃ"  
³ M3,4 T1 G1,2,4 PPS;T2 "nārāyaṇaparaṃ padam"; M1 "nārāyaṇaparāparam"; M2 "nārāyaṇaparaṃ varam";
  CE "sarvaṃ nārāyaṇātmakam"; Dn "sarvaṃ nārāyaṇaḥ sadā"

Above // is CE@97

> yathā viṣṇumayaṃ satyaṃ yathā viṣṇumayaṃ ¹haviḥ  
> yathā viṣṇumayaṃ sarvaṃ pāpmā mē naṡyatāṃ tathā  

¹ all; Dn "haviḥ"

> tvāṃ prapannāya bhaktāya gatim iṣṭāṃ ¹jigīṣavē  
> yac chrēyaḥ puṇḍarīkākṣa tad dhyāyasva surōttama  

¹ M Dn CE; T G PPS "jigīṣatē"

Above // is 12.51.9

> iti vidyātapōyōnir ayōnir viṣṇur īḍitaḥ  
> vāg¹yajn̄ēna parō dēvaḥ prīyatāṃ mē janārdanaḥ  

¹ M T1 G4; et al. "-yajn̄ēnārcitō dē-"

>    vaiṡaṃpāyana uvāca  
> ētāvad uktvā vacanaṃ bhīṣmas tadgatamānasaḥ  
> nama ity ēva kṛṣṇāya praṇāmam akarōt tadā  

> ¹adhigamya tu yōgēna bhaktiṃ bhīṣmasya mādhavaḥ  
> ²trailōkyadarṡanaṃ jn̄ānaṃ divyaṃ ²datvā yayau hariḥ  

¹ M1,4 T G1,2,4 PPS; et al. "abhigamya"  
² M T G Dn PPS; CE "traikālya-"  
³ M T G Dn PPS: CE "dātuṃ"

<!-- M1,3 in. CE@98 here; L3-4 of it are in. by D7 T G1,2 at the end of chapter (see CE@99L9,10) -->

> tasminn uparatē ¹vākyē tatas tē brahmavādinaḥ  
> bhīṣmaṃ vāgbhir bāṣpa²kalās ³tam ānarcur ⁴mahāmatim  

¹ M T G PPS; Dn CE "ṡabdē"  
² M; T G PPS "-galās"; Dn CE "-kaṇṭhās"  
³ M2,4 T G CE PPS; M1,3 "samānarcur"  
⁴ M3,4 T G CE PPS; M1,2 "mahāpatim"

> tē stuvantaṡ ca ¹viprāgryāḥ kēṡavaṃ puruṣōttamam  
> bhīṣmaṃ ca ṡanakaiḥ sarvē praṡaṡaṃsuḥ punaḥ punaḥ  

¹ M Dn CE; T G PPS "viprēndrāḥ"

> viditvā bhaktiyōgaṃ ¹taṃ bhīṣmasya puruṣōttamaḥ  
> sahasōtthāya ²saṃhṛṣṭō yānam ēvānvapadyata  

¹ M T G PPS; Dn CE "tu"  
² M T2 G4 Dn CE; T1 G1,2 PPS "taṃ hṛṣṭō"

> kēṡavaḥ sātyakiṡ caiva rathēnaikēna jagmatuḥ  
> aparēṇa mahātmānau yudhiṣṭhiradhanaṃjayau  

> bhīmasēnō yamau cōbhau ratham ēkaṃ ¹samāsthitāḥ  
> kṛpō yuyutsuḥ sūtaṡ ca saṃjayaṡ cāparaṃ ratham  

¹ M T G B PPS; Dn "samāṡritāḥ"; CE "samāsthitau"

> tē rathair nagarākāraiḥ prayātāḥ puruṣarṣabhāḥ  
> nēmighōṣēṇa mahatā kampayantō vasuṃdharām  

> tatō giraḥ puruṣavaras ¹tavānvitā  
>  dvijēritāḥ ²pathi sumanāḥ sa ṡuṡruvē  
> kṛtān̄jaliṃ praṇatam athāparaṃ janaṃ  
>  sa kēṡihā muditamanābhyanandata  

¹ M Dn CE; T G PPS "tavānvitā"  
² M T2 G2 Dn CE; T1 G1,4 PPS "pathiṣu manāk"
